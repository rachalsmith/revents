import React from 'react';
import { useState } from 'react';
import { Container } from 'semantic-ui-react';

import EventDashboardContainer from '../../features/events/eventDashboardContainer/EventDashboardContainer';
import NavBar from '../../features/nav/NavBar';

import './styles.css';

function App() {

  const [formOpen, setFormOpen] = useState(false);


  return (
    <>
      <NavBar setFormOpen={ setFormOpen } />
        <Container className='main'>
          <EventDashboardContainer formOpen={ formOpen } setFormOpen={ setFormOpen } />
        </Container>
    </>
  );
}

export default App;
